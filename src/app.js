import Header from './components/Header'
import File from './classes/File'
import { Process } from './helpers'

async function App() {

  const file1 = new File(1);
  const file2 = new File(2);

  const template = document.createElement('template')
  template.innerHTML = `
    <div class="container">
      ${Header()}
      ${file1.getTemplate()}
      ${file2.getTemplate()}
      <button type="button" id="process" class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 my-4 px-4">Join and Download</button>
    </div>
  `
  document.addEventListener('DOMContentLoaded', function() {
    document.getElementById("process").addEventListener("click", () => { Process(file1,file2)  });
    document.getElementById('xlf1').addEventListener("change", (evt) => { file1.readFile(evt)  } , true);
    document.getElementById('xlf2').addEventListener("change", (evt) => { file2.readFile(evt)  } , true);
  });

  // Return a new node from template
  return template.content.cloneNode(true)
}

export default App;