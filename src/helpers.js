//import axios from 'axios';

import XLSX from 'xlsx';

export const Process = async (file1,file2) => {
	//const res = await axios.get('https://randomuser.me/api');
	//const user = res.data.results[0];


    //return template;
    console.log(file1.headerRow);
    console.log(file2.headerRow);
    console.log(file1.unique);
    console.log(file2.unique);

    console.log(document.getElementById('fields'+file2.unique).value);
    console.log(document.getElementById('fields'+file1.unique).value);

};

export const getHeaders = (workbook) => {
    const firstSheeName = workbook.SheetNames[0];
    const sheet = workbook.Sheets[firstSheeName];

    let headers = [];
    const range = XLSX.utils.decode_range(sheet['!ref']);
    //start in the first row
    let C, R = range.s.r;
    //walk every column in the range
    for(C = range.s.c; C <= range.e.c; ++C) {
        //find the cell in the first row 
        let cell = sheet[XLSX.utils.encode_cell({c:C, r:R})] 

        let hdr = "UNKNOWN " + C;
        if(cell && cell.t) hdr = XLSX.utils.format_cell(cell);

        headers.push(hdr);
    }
    return headers;
}
