const Header = () => {
	const template = `
    <header>
      <h1>Between Two Sheets</h1>
      <p>Join two spreadsheets on common field 100% in your browser.</p>
    </header>
  `;

	return template;
};

export default Header;
