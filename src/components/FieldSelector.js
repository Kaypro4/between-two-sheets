const FieldSelector = () => {

	const template = `
    <div class="field">

      <select name="field">
        <option value="1">Name</option>
        <option value="2">Id</option>
      </select>
    </div>
  `;

	return template;
};

export default FieldSelector;
