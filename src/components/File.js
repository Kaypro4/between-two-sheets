import XLSX from 'xlsx';
import { getHeaders } from '../helpers';

const File = (unique) => {

    function handleFile(e) {
        var files = e.target.files, f = files[0];
        var reader = new FileReader();
        reader.onload = (e) => {
            var data = new Uint8Array(e.target.result);
            var workbook = XLSX.read(data, {type: 'array'});

            //get header row from file
            let headerRow = getHeaders(workbook);

            //populate select with header fields
            const select = document.getElementById('fields'+unique);
            select.innerHTML = '';

            for (let i = 0; i < headerRow.length; i++) {
                let opt = document.createElement('option');
                opt.value = headerRow[i];
                opt.innerHTML = headerRow[i]; 
                select.appendChild(opt);
            } 

        };
        reader.readAsArrayBuffer(f);
    }

    document.addEventListener('DOMContentLoaded', function() {
        var xlf = document.getElementById('xlf'+unique);
        xlf.addEventListener('change', handleFile, false);
    });

    const template = `
    <div class="file">
      <input type="file" name="xlfile" id="xlf${unique}" />
      <select name="field" id="fields${unique}">
        <option value="Field">Field</option>
      </select>
    </div>
    `;

	return template;
};

export default File;
