import XLSX from 'xlsx';
import { getHeaders } from '../helpers';

export default class File {
  constructor(unique) {
    this.unique = unique;
    this.workbook = null;
    this.headerRow = null;
  }
  getTemplate() {
    const template = `
        <div class="file">
        <input type="file" name="xlfile" id="xlf${this.unique}" />
        <select name="field" id="fields${this.unique}">
            <option value="Field">Field</option>
        </select>
        </div>
    `;
    return template;
  }
  saveFileDetails(workbook) {

    this.workbook = workbook;
    //get header row from file
    this.headerRow = getHeaders(this.workbook);

    //populate select with header fields
    let select = document.getElementById(`fields${this.unique}`);
    select.innerHTML = '';

    for (let i = 0; i < this.headerRow.length; i++) {
        let opt = document.createElement('option');
        opt.value = this.headerRow[i];
        opt.innerHTML = this.headerRow[i]; 
        select.appendChild(opt);
    } 
  }
  readFile(e) {
        let files = e.target.files, f = files[0];
        let reader = new FileReader();
        reader.onload = (e) => {

            let data = new Uint8Array(e.target.result);
            let workbook = XLSX.read(data, {type: 'array'});

            this.saveFileDetails(workbook);

        };
        reader.readAsArrayBuffer(f);
    }
}